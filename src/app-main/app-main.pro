TEMPLATE = app
TARGET = OpenGLPlayground

QT += core gui opengl
LIBS += -lopengl32

CONFIG += app_bundle

SOURCES += main.cpp \
            mainwindow.cpp \

HEADERS += mainwindow.h

DEPENDENCY_LIBRARIES += app-lib
include(../qmake/includes.pri)
