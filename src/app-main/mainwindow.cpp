#include "mainwindow.h"

MainWindow::MainWindow() {
    qInfo() << "Open Object File";
    QString fileName = QFileDialog::getOpenFileName(nullptr, "Open a Object File", QDir::homePath(), "Object File (*.ply)");
    currentObject = Object3D::import(fileName);

    setSurfaceType(QWindow::OpenGLSurface);

    QSurfaceFormat format;
    format.setProfile(QSurfaceFormat::CompatibilityProfile);
    format.setVersion(2, 1);
    setFormat(format);

    context = new QOpenGLContext;
    context->setFormat(format);
    context->create();
    context->makeCurrent(this);

    openGLFunctions = context->functions();

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(UpdateAnimation()));

    timer->start(15);

    rotation = 0;
}

MainWindow::~MainWindow() {

}

void MainWindow::initializeGL() {
    glEnable(GL_DEPTH_TEST);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    resizeGL(this->width(), this->height());
}

void perspectiveGL( GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar ) {
    const GLdouble pi = 3.1415926535897932384626433832795;
    GLdouble fW, fH;

    fH = tan( fovY / 360 * pi ) * zNear;
    fW = fH * aspect;

    glFrustum( -fW, fW, -fH, fH, zNear, zFar );
}

void MainWindow::resizeGL(int w, int h) {
    // set viewport
    glViewport(0,0, w, h);

    qreal aspectratio = qreal(w) / qreal(h);

    // init projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    perspectiveGL(75, aspectratio, 0.1, 400000000);

    // init model view matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void MainWindow::paintGL() {
    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // reset model view matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // 3D transformation
    glTranslatef(0.0, 0.0, -3.0);
    glRotatef(rotation, 1.0, 0.1, 0.1);

    currentObject.draw();

    glFlush();
}

void MainWindow::resizeEvent(QResizeEvent *event) {
    resizeGL(this->width(), this->height());
    this->update();
}

void MainWindow::paintEvent(QPaintEvent *event) {
    paintGL();
    qInfo() << QTime::currentTime().msec() - lastFrameTime.msec();

    lastFrameTime = QTime::currentTime();
}

void MainWindow::UpdateAnimation() {
    rotation += 1;

    this->update();
}
