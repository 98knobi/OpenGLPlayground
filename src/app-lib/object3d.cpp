#include "object3d.h"

namespace Object3D {

Object::Object(int polyCount_in, std::vector<Polygon> polygons_in) {
    polyCount = polyCount_in;
    polygons = polygons_in;
}

void Object::draw() {
    for (int i=0; i<polyCount; i++) {
        polygons[i].draw();
    }
}


Object import(QString path) {

    // get the file content
    QString fileName = path;
    QFile file(fileName);

    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        qDebug() << "could not open the file";
        return Object();
    }

    QTextStream in(&file);
    QString fileText = in.readAll();

    // file no longer needed
    file.close();

    return importPLY(fileText);
}

Object importPLY(QString plyFileContent) {
    qInfo() << "ply importer startet";

    QStringList fileLines = plyFileContent.split('\n');

    int headerEnd = fileLines.indexOf("end_header");
    int vertexCount = fileLines[fileLines.indexOf(QRegExp("element vertex [0-9]+"))].split(' ').last().toInt();
    int faceCount = fileLines[fileLines.indexOf(QRegExp("element face [0-9]+"))].split(' ').last().toInt();

    QRegularExpression rx("property (char|uchar|short|ushort|int|uint|float|double) ([a-z]+)");
    QRegularExpressionMatchIterator rxi = rx.globalMatch(plyFileContent);

    struct vertexProp {
        std::string propType;
        std::string prop;
    };

    std::vector<vertexProp> vertexProperties;

    while (rxi.hasNext()) {
        QRegularExpressionMatch match = rxi.next();
        if (match.hasMatch()) {

            QString propType = match.captured(2);

            // filter coord props
            if (propType == "x") {
                vertexProperties.push_back({"coord", "x"});
            }
            else if (propType == "y") {
                vertexProperties.push_back({"coord", "y"});
            }
            else if (propType == "z") {
                vertexProperties.push_back({"coord", "z"});
            }

            // filter normal props
            else if (propType == "nx") {
                vertexProperties.push_back({"normal", "x"});
            }
            else if (propType == "ny") {
                vertexProperties.push_back({"normal", "y"});
            }
            else if (propType == "nz") {
                vertexProperties.push_back({"normal", "z"});
            }

            // filter color props
            else if (propType == "red") {
                vertexProperties.push_back({"color", "r"});
            }
            else if (propType == "green") {
                vertexProperties.push_back({"color", "g"});
            }
            else if (propType == "blue") {
                vertexProperties.push_back({"color", "b"});
            }

            // filter color props
            else if (propType == "s") {
                vertexProperties.push_back({"uvcoord", "x"});
            }
            else if (propType == "t") {
                vertexProperties.push_back({"uvcoord", "y"});
            }
        }
    }

    std::vector<Vertice> verts;
    for (int i=headerEnd+1; i<vertexCount+headerEnd+1; i++) {
        Vertice workVert;

        QStringList components = fileLines[i].split(' ');

        for (int i=0; i<components.length(); i++) {
            vertexProperties[i];
            if (vertexProperties[i].propType == "coord") {
                if (vertexProperties[i].prop == "x") {
                    workVert.coords.x = components[i].toFloat();
                }
                else if (vertexProperties[i].prop == "y") {
                    workVert.coords.y = components[i].toFloat();
                }
                else if (vertexProperties[i].prop == "z") {
                    workVert.coords.z = components[i].toFloat();
                }
            }

            else if (vertexProperties[i].propType == "normal") {
                if (vertexProperties[i].prop == "x") {
                    workVert.normal.x = components[i].toFloat();
                }
                else if (vertexProperties[i].prop == "y") {
                    workVert.normal.y = components[i].toFloat();
                }
                else if (vertexProperties[i].prop == "z") {
                    workVert.normal.z = components[i].toFloat();
                }
            }
            else if (vertexProperties[i].propType == "color") {
                if (vertexProperties[i].prop == "r") {
                    workVert.vertColor.r = components[i].toInt();
                }
                else if (vertexProperties[i].prop == "g") {
                    workVert.vertColor.g = components[i].toInt();
                }
                else if (vertexProperties[i].prop == "b") {
                    workVert.vertColor.b = components[i].toInt();
                }
            }
            else if (vertexProperties[i].propType == "uv") {
                if (vertexProperties[i].prop == "x") {
                    workVert.uv.x = components[i].toFloat();
                }
                else if (vertexProperties[i].prop == "y") {
                    workVert.uv.y = components[i].toFloat();
                }
            }

            workVert.vertColor.a = 255;
        }
        verts.push_back(workVert);
    }

    //Polygon polys[faceCount];
    std::vector<Polygon> polys;
    for (int i=headerEnd+vertexCount+1; i<faceCount+vertexCount+headerEnd+1; i++) {
        //qDebug() << fileLines[i];
        QStringList components = fileLines[i].split(' ');

        int polyVertCount = components[0].toInt();
        //Vertice polyVerts[polyVertCount];
        std::vector<Vertice> polyVerts;

        for (int j=0; j<polyVertCount; j++) {
            //polyVerts[j] = verts[components[j+1].toInt()];
            polyVerts.push_back(verts[components[j+1].toInt()]);
        }

        //polys[i-(headerEnd+vertexCount+1)] = Polygon(polyVertCount, polyVerts);
        polys.push_back(Polygon(polyVertCount, polyVerts));
    }

    qDebug() << "loaded with " << faceCount << " faces and " << vertexCount << " vertices";

    return Object(faceCount, polys);
}

}

