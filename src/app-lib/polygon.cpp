#include "polygon.h"

namespace Object3D {

std::ostream & operator << (std::ostream & os, const Vector2 & data) {
    return os << "(" << data.x << ";" << data.y << ")";
}

std::ostream & operator << (std::ostream & os, const Vector3 & data) {
    return os << "(" << data.x << ";" << data.y << ";" << data.z << ")";
}

std::ostream & operator << (std::ostream & os, const ColorB & data) {
    return os << "(" << data.r << ";" << data.g << ";" << data.b << ";" << data.a << ")";
}

std::ostream & operator << (std::ostream & os, const Vertice & data) {
    return os << "coords: " << data.coords
              << "normals: " << data.normal
              << "color: " << data.vertColor;
}

QDebug operator << (QDebug d, const Vector2 & data) {
    return d << "(" << data.x << ";" << data.y << ")";
}

QDebug operator << (QDebug d, const Vector3 & data) {
    return d << "(" << data.x << ";" << data.y << ";" << data.z << ")";
}

QDebug operator << (QDebug d, const ColorB & data) {
    return d << "(" << data.r << ";" << data.g << ";" << data.b << ";" << data.a << ")";
}

QDebug operator << (QDebug d, const Vertice & data) {
    return d << "coords: " << data.coords
              << "normals: " << data.normal
              << "color: " << data.vertColor;
}

bool Vector2::operator ==(const Vector2 &other) const {
    return (this->x == other.x
            && this->y == other.y);
}

bool Vector3::operator ==(const Vector3 &other) const {
    return (this->x == other.x
            && this->y == other.y
            && this->z == other.z);
}

bool Color::operator ==(const Color &other) const {
    return (this->r == other.r
            && this->g == other.g
            && this->b == other.b
            && this->a == other.a);
}

bool ColorB::operator ==(const ColorB &other) const {
    return (this->r == other.r
            && this->g == other.g
            && this->b == other.b
            && this->a == other.a);
}

bool Vertice::operator ==(const Vertice &other) const {
    return (this->coords == other.coords
            && this->normal == other.normal
            && this->vertColor == other.vertColor
            && this->uv == other.uv);
}

bool Vertice::operator !=(const Vertice &other) const{
    return !(*this == other);
}

Polygon::Polygon(int vertCount_in, std::vector<Vertice> vertices_in) {
    vertCount = vertCount_in;
    vertices = vertices_in;
}

void Polygon::draw() {
    if (vertCount > 3)
        glBegin(GL_POLYGON);
    else
        glBegin(GL_TRIANGLES);

    for (int i=0; i<vertCount; i++) {
        glColor4ub(vertices[i].vertColor.r,vertices[i].vertColor.g,vertices[i].vertColor.b, vertices[i].vertColor.a);
        glNormal3f(vertices[i].normal.x, vertices[i].normal.y, vertices[i].normal.z);
        glVertex3f(vertices[i].coords.x, vertices[i].coords.y, vertices[i].coords.z);
    }

    glEnd();
}

std::vector<Vertice> Polygon::getVertices() const {
    return vertices;
}

}
