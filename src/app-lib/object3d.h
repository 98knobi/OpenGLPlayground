#ifndef OBJECT3D_H
#define OBJECT3D_H

#include <vector>
#include <QtOpenGL>
#include <QFile>
#include <QtGlobal>
#include <iostream>

#include "polygon.h"

namespace Object3D {

class Object {
private:
    std::vector<Object3D::Polygon> polygons;
    int polyCount;

public:
    Object(int polyCount_in = 0, std::vector<Object3D::Polygon> polygons_in = {});
    void draw();
};

Object import(QString path);
Object importPLY(QString plyFileContent);

}

#endif // OBJECT3D_H
