#ifndef POLYGON_H
#define POLYGON_H

#include <vector>
#include <QtOpenGL>

#include <QtGlobal>
#include <iostream>

namespace Object3D {

struct Vector2 {
    float x;
    float y;

public:
    bool operator == (const Vector2 &other) const;
    bool operator != (const Vector2 &other) const;
};

struct Vector3 {
    float x;
    float y;
    float z;

public:
    bool operator == (const Vector3 &other) const;
    bool operator != (const Vector3 &other) const;
};

struct Color {
    float r;
    float g;
    float b;
    float a;

public:
    bool operator == (const Color &other) const;
    bool operator != (const Color &other) const;
};

struct ColorB {
    byte r;
    byte g;
    byte b;
    byte a;

public:
    bool operator == (const ColorB &other) const;
    bool operator != (const ColorB &other) const;
};

struct Vertice {
    Vector3 coords;
    Vector3 normal;
    ColorB vertColor;
    Vector2 uv;

public:
    bool operator == (const Vertice &other) const;
    bool operator != (const Vertice &other) const;
};

class Polygon {
private:
    //std::vector<Vertice> vertices;
    std::vector<Vertice> vertices;
    int vertCount;

public:
    Polygon(int vertCount_in = 0, std::vector<Vertice> vertices_in = {});
    void draw();
    std::vector<Vertice> getVertices() const;
};

}

#endif // POLYGON_H
