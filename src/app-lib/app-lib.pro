include(../qmake/common.pri)

QT += opengl
LIBS += -lopengl32

TARGET = app-lib
TEMPLATE = lib

HEADERS += polygon.h \
            object3d.h \

SOURCES += polygon.cpp \
            object3d.cpp \

