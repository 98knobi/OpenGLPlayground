#-------------------------------------------------
#
# Project created by QtCreator 2018-07-08T14:27:53
#
#-------------------------------------------------

TEMPLATE = subdirs
SUBDIRS = app-main \
            app-lib \
            googletest \            
            unittest

app-main.depends = app-lib
unittest.depends = app-lib googletest

OTHER_FILES = ../README.md
