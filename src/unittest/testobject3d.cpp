#include <gtest/gtest.h>

#include "app-lib/object3d.h"

//using namespace Object3D;

TEST(Object3D, TestPolygonVerticePositions) {
    //FAIL();
    // Setup
    std::vector<Object3D::Vertice> testVertices = {
        {{1,1,1}, {1,1,1}, {0,0,0, 255}, {1,1}},
        {{2,2,2}, {2,2,2}, {128,128,128, 255}, {2,2}},
        {{3,3,3}, {3,3,3}, {255,255,255, 255}, {3,3}}
    };

    Object3D::Polygon testpoly(3, testVertices);

    // Assert
    EXPECT_EQ(testpoly.getVertices(), testVertices);
}
