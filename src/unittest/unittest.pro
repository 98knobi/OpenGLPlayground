#-------------------------------------------------
#
# Project created by QtCreator 2018-07-22T12:10:15
#
#-------------------------------------------------

TARGET = unit-test
TEMPLATE = app

QT       += testlib
QT       -= gui
QT      += opengl

CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += testcase

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    testmain.cpp \
    testobject3d.cpp

# allow to #include <gtest/...> and <gmock/..>
INCLUDEPATH += $$_PRO_FILE_PWD_/../googletest

DEPENDENCY_LIBRARIES += googletest \    # used in includes.pri
    app-lib
include(../qmake/includes.pri)
