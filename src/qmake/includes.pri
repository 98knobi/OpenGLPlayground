include(../qmake/common.pri)

INCLUDEPATH += ..

# This function sets up the dependencies for libraries that are built with
# this project.  Specify the libraries you need to depend on in the variable
# DEPENDENCY_LIBRARIES and this will add.
for(dep, DEPENDENCY_LIBRARIES) {
    LIBS += $${DESTDIR}/$${QMAKE_PREFIX_STATICLIB}$${dep}.$${QMAKE_EXTENSION_STATICLIB}
    PRE_TARGETDEPS += $${DESTDIR}/$${QMAKE_PREFIX_STATICLIB}$${dep}.$${QMAKE_EXTENSION_STATICLIB}
}
