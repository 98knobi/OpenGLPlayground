# OpenGL Playground
## Description

Wanted to build something with C++ and OpenGL.

This Project is for me, so i can fiddle a bit around.

I use C++ because it's the common choice for OpenGL Programms.\
Additionally i use the QT libraries, because they make working with C++ a bit nicer.

## Current Feature set
I have a Namespace wich holds some Classes, Functions and Structs to create and Render 3D Objects.

I have a simple Import Function wich can handle text based 3D Object Files.\
Currently Supported File formats:
* ply 